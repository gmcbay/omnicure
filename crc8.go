package omnicure

func calcCRC8(data []byte) (crc8 byte) {
	for i := 0; i < len(data); i++ {
		a := data[i]

		for j := 0; j < 8; j++ {
			if (a^crc8)&0x01 != 0 {
				crc8 ^= 0x18
				crc8 >>= 1
				crc8 |= 0x80
			} else {
				crc8 >>= 1
			}

			a >>= 1
		}
	}

	return
}
