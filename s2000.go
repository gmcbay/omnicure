package omnicure

import (
	"bytes"
	"code.google.com/p/serial"
	"fmt"
	"io"
)

type S2000 struct {
	serial     io.ReadWriteCloser
	fullBuffer []byte
}

func NewS2000(comPort string) (s2000 *S2000, err error) {
	s2000 = new(S2000)

	if s2000.serial, err = serial.OpenPort(
		&serial.Config{Name: comPort, Baud: 19200}); err != nil {
		return
	}

	return
}

func (s2000 *S2000) Cleanup() {
	s2000.serial.Close()
	s2000.serial = nil
	s2000.fullBuffer = nil
}

func (s2000 *S2000) SendCommand(cmd string) (result string, err error) {
	sendData := fmt.Sprintf("%s%02X\r", cmd, calcCRC8([]byte(cmd)))

	fmt.Printf("Sending: %s\n", sendData)

	if _, err = s2000.serial.Write([]byte(sendData)); err != nil {
		return
	}

	readBuffer := make([]byte, 1)

	for {
		if _, err = s2000.serial.Read(readBuffer); err != nil {
			return
		}

		s2000.fullBuffer = append(s2000.fullBuffer, readBuffer...)

		// Look for \r (carriage return)
		cr := bytes.IndexByte(s2000.fullBuffer, 13)

		if cr != -1 {
			resultData := string(s2000.fullBuffer[0:cr])

			if len(resultData) < 3 {
				err = fmt.Errorf("Invalid result: %v\n", resultData)
				return
			}

			if len(s2000.fullBuffer) > cr {
				s2000.fullBuffer = s2000.fullBuffer[(cr + 1):]
			} else {
				s2000.fullBuffer = nil
			}

			result = resultData[0 : len(resultData)-2]
			crc := resultData[len(resultData)-2:]
			checkCrc := fmt.Sprintf("%02X", calcCRC8([]byte(result)))

			if crc != checkCrc {
				err = fmt.Errorf("Unexpected CRC: %v (%v expected)", crc, checkCrc)
			}

			fmt.Printf("Read: %v\n", result)

			return
		}
	}

	return
}
